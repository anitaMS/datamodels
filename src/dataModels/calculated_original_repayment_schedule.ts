/**
 * DESCRIPTION: CALCULATE_ORIGINAL_REPAYMENT_SCHEDULE DATA MODEL USED
 *              TO FETCH THE CUSTOMER DETAILS
 * CREATED BY: VIKAS
 * CREATED DATE: 28/04/2021
 */

import { DataTypes, Model } from "sequelize";
import { DbConnection } from "../util/dbConfig";
const sequelize = new DbConnection().connectMySql();

// We recommend you declare an interface for the attributes, for stricter typechecking
interface OriginalRepaymentScheduleInstance extends Model {
  loan_id: number;
  installment_date: Date;
  installment_amount: number;
  demand_no: number;
  principal_amount: number;
  interest_amount: number;
  calculated_principal_outstanding_amount: number;
}

// Define calculated_original_repayment_schedule ORM data model object
const calculated_original_repayment_schedule =
  sequelize.define<OriginalRepaymentScheduleInstance>(
    "calculated_original_repayment_schedule",
    {
      loan_id: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      installment_date: {
        type: DataTypes.DATEONLY,
        allowNull: true,
      },
      installment_amount: {
        type: DataTypes.FLOAT(12, 2),
        allowNull: true,
      },
      demand_no: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      principal_amount: {
        type: DataTypes.FLOAT(12, 2),
        allowNull: true,
      },
      interest_amount: {
        type: DataTypes.FLOAT(12, 2),
        allowNull: true,
      },
      calculated_principal_outstanding_amount: {
        type: DataTypes.FLOAT(12, 2),
        allowNull: true,
      },
    },
    {
      timestamps: false,
      freezeTableName: true,
    }
  );

// Remove the attributes that are auto created by Sequelize
calculated_original_repayment_schedule.removeAttribute("id"); // Primary Key Attribute
calculated_original_repayment_schedule.removeAttribute("createdAt"); // Created Date Attribute
calculated_original_repayment_schedule.removeAttribute("updatedAt"); // Update Date Attribute

/**
 * This async method will create a new table
 * with the above model else will update the
 * record in the table.
 */
// (async () => {
//   await calculated_original_repayment_schedule.sync();
// })();

export = calculated_original_repayment_schedule;
