/**
 * CREATED BY: ANITA MS
 * CREATED DATE: 10/05/2021
 * MODIFIED BY:
 * MODIFIED DATE:
 * DESCRIPTION: AGG_MLY_LMS_ALM_DETAILS data model used to
 *              Define AGG_MLY_LMS_ALM_DETAILS ORM data model object and
 *              to fetch the ALM details (Asset and liability management)
 */

import { DataTypes, Model } from "sequelize";
import { DbConnection } from "../util/dbConfig";
const sequelize = new DbConnection().connectMySql();

// We recommend you declare an interface for the attributes, for stricter typechecking
interface ALMAPIInstance extends Model {
  date_key: number;
  particular_type: string;
  particular_category: string;
  component: string;
  one_month: number;
  two_month: number;
  three_month: number;
  four_month: number;
  five_month: number;
  six_month: number;
  six_twelve_month: number;
  one_three_year: number;
  three_five_years: number;
  five_plus_years: number;
  total_amount: number;
}

// Define AGG_MLY_LMS_ALM_DETAILS ORM data model object
const AGG_MLY_LMS_ALM_DETAILS = sequelize.define<ALMAPIInstance>(
  "AGG_MLY_LMS_ALM_DETAILS",
  {
    date_key: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    particular_type: {
      type: DataTypes.STRING(20),
      allowNull: true,
    },
    particular_category: {
      type: DataTypes.STRING(100),
      allowNull: true,
    },
    component: {
      type: DataTypes.STRING(20),
      allowNull: true,
    },
    one_month: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    two_month: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    three_month: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    four_month: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    five_month: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    six_month: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    six_twelve_month: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    one_three_year: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    three_five_years: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    five_plus_years: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    total_amount: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
  },
  {
    timestamps: false,
    freezeTableName: true,
  }
);

// Remove the attributes that are auto created by Sequelize
AGG_MLY_LMS_ALM_DETAILS.removeAttribute("id"); // Primary Key Attribute
AGG_MLY_LMS_ALM_DETAILS.removeAttribute("createdAt"); // Created Date Attribute
AGG_MLY_LMS_ALM_DETAILS.removeAttribute("updatedAt");

export = AGG_MLY_LMS_ALM_DETAILS;
