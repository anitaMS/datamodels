/**
 * CREATED BY: ANITA MS
 * CREATED DATE: 10/05/2021
 * MODIFIED BY:
 * MODIFIED DATE:
 * DESCRIPTION: FCT_LMS_LOAN_DETAILS data model used to  
 *              Define FCT_LMS_LOAN_DETAILS ORM data model object and 
 *              to fetch the Lender Information
 */

 import { DataTypes, Model } from "sequelize";
import { DbConnection } from "../util/dbConfig";
const sequelize = new DbConnection().connectMySql();
 
// We recommend you declare an interface for the attributes, for stricter typechecking
interface LenderInfoAPIInstance extends Model {
    Lender_Name: string,
      Lender_ID: string,
      Loan_Nick_Name: string,
      Sanctioned_Amount: number,
      Loan_Amount:number,
      Sanctioned_Date: Date,
      Disbursement_Date: Date,
      Listed_Unlisted: string,
      Type_of_Product: string,
      Type_of_Entity: string,
      Secured_Unsecured: string,
      Arranger: string,
      Interest_Rate: string,
      Tenure_months:number,
      Processing_Fees: string,
      Cash_Collateral: string,
      Arranger_Fees: string,
      Other_Fees: string,
      Book_Debt: string,
      Interest_Rate_Type: string,
      Principal_Repayment_Type: string,
      Interest_Repayment_Type: string,
      "1stCall_Put_Option": Date,
      "2ndCall_Put_Option": Date,
      "3rdCall_Put_Option": Date,
      Primary_Redemption_Date: Date,
      Final_Redemption_Date: Date,
      Status: string,
      XIRR_Percent: string,
      LIDID: string,
  }
  

 // Define FCT_LMS_LOAN_DETAILS ORM data model object
 const FCT_LMS_LOAN_DETAILS = sequelize.define<LenderInfoAPIInstance>(
   "FCT_LMS_LOAN_DETAILS",
   {
     Lender_Name: {
       type: DataTypes.STRING(100),
       allowNull: true,
     },
     Lender_ID: {
       type: DataTypes.STRING(50),
       allowNull: true,
     },
     Loan_Nick_Name: {
       type: DataTypes.STRING(100),
       allowNull: true,
     },
     Sanctioned_Amount: {
       type: DataTypes.INTEGER,
       allowNull: true,
     },
     Loan_Amount: {
       type: DataTypes.INTEGER,
       allowNull: true,
     },
     Sanctioned_Date: {
       type: DataTypes.DATE,
       allowNull: true,
     },
     Disbursement_Date: {
       type: DataTypes.DATE,
       allowNull: true,
     },
     Listed_Unlisted: {
       type: DataTypes.STRING(50),
       allowNull: true,
     },
     Type_of_Product: {
       type: DataTypes.STRING(50),
       allowNull: true,
     },
     Type_of_Entity: {
       type: DataTypes.STRING(50),
       allowNull: true,
     },
     Secured_Unsecured: {
       type: DataTypes.STRING(50),
       allowNull: true,
     },
     Arranger: {
       type: DataTypes.STRING(50),
       allowNull: true,
     },
     Interest_Rate: {
       type: DataTypes.STRING(50),
       allowNull: true,
     },
     Tenure_months: {
       type: DataTypes.INTEGER,
       allowNull: true,
     },
     Processing_Fees: {
       type: DataTypes.STRING(50),
       allowNull: true,
     },
     Cash_Collateral: {
       type: DataTypes.STRING(50),
       allowNull: true,
     },
     Arranger_Fees: {
       type: DataTypes.STRING(50),
       allowNull: true,
     },
     Other_Fees: {
       type: DataTypes.STRING(50),
       allowNull: true,
     },
     Book_Debt: {
       type: DataTypes.STRING(50),
       allowNull: true,
     },
     Interest_Rate_Type: {
       type: DataTypes.STRING(50),
       allowNull: true,
     },
     Principal_Repayment_Type: {
       type: DataTypes.STRING(50),
       allowNull: true,
     },
     Interest_Repayment_Type: {
       type: DataTypes.STRING(50),
       allowNull: true,
     },
     "1stCall_Put_Option": {
       type: DataTypes.DATE,
       allowNull: true,
     },
     "2ndCall_Put_Option": {
       type: DataTypes.DATE,
       allowNull: true,
     },
     "3rdCall_Put_Option": {
       type: DataTypes.DATE,
       allowNull: true,
     },
     Primary_Redemption_Date: {
       type: DataTypes.DATE,
       allowNull: true,
     },
     Final_Redemption_Date: {
       type: DataTypes.DATE,
       allowNull: true,
     },
     Status: {
       type: DataTypes.STRING(50),
       allowNull: true,
     },
     XIRR_Percent: {
       type: DataTypes.STRING(50),
       allowNull: true,
     },
     LIDID: {
       type: DataTypes.STRING(20),
       allowNull: true,
     },
   },
   {
     timestamps: false,
     freezeTableName: true,
   }
 );
 
 // Remove the attributes that are auto created by Sequelize
 FCT_LMS_LOAN_DETAILS.removeAttribute("id"); // Primary Key Attribute
 FCT_LMS_LOAN_DETAILS.removeAttribute("createdAt"); // Created Date Attribute
 FCT_LMS_LOAN_DETAILS.removeAttribute("updatedAt");
 
 export = FCT_LMS_LOAN_DETAILS;
 