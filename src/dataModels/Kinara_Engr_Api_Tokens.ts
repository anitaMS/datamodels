/**
 * DESCRIPTION: KINARA_ENGR_API_TOKENS DATA MODEL
 *              USED TO CAPTURES THE USER TOKENS
 *              WHO HAVE LOGGED-IN
 * CREATED BY: VIKAS
 * CREATED DATE: 10/05/2021
 */

import { DataTypes, Model } from "sequelize";
import { DbConnection } from "../util/dbConfig";
const sequelize = new DbConnection().connectMySql();

// Define Kinara_Engr_Api_Tokens ORM data model object
const Kinara_Engr_Api_Tokens = sequelize.define(
  "Kinara_Engr_Api_Tokens",
  {
    user_Id: {
      type: DataTypes.INTEGER,
    },
    token: {
      type: DataTypes.STRING(2500),
    },
    token_status: {
      type: DataTypes.STRING(50),
    },
    created_timest: {
      type: DataTypes.DATE,
    },
  },
  {
    timestamps: false,
  }
);

/**
 * This async method will create a new table
 * with the above model else will update the
 * record in the table.
 */
// (async () => {
//   await Kinara_Engr_Api_Tokens.sync();
// })();

export = Kinara_Engr_Api_Tokens;
