/**
 * DESCRIPTION: KINARA_ENGR_API_HIST_TABLE DATA MODEL USED
 *              TO LOG THE API HITS
 * CREATED BY: VIKAS
 * CREATED DATE: 10/05/2021
 */

import { DataTypes, Model } from "sequelize";
import { DbConnection } from "../util/dbConfig";
const sequelize = new DbConnection().connectMySql();

// Define customer_api ORM data model object
const kinara_engr_api_hist_table = sequelize.define(
  "kinara_engr_api_hist_table",
  {
    application_id: {
      type: DataTypes.TEXT,
    },
    user_id: {
      type: DataTypes.TEXT,
    },
    api_route: {
      type: DataTypes.TEXT,
    },
    api_name: {
      type: DataTypes.TEXT,
    },
    api_error_label: {
      type: DataTypes.TEXT,
    },
    status_code: {
      type: DataTypes.BIGINT,
    },
    api_status: {
      type: DataTypes.TEXT,
    },
    logic_status: {
      type: DataTypes.TEXT,
    },
    api_request: {
      type: DataTypes.TEXT,
    },
    api_response: {
      type: DataTypes.TEXT,
    },
    server_type: {
      type: DataTypes.TEXT,
    },
    api_request_on: {
      type: DataTypes.DATE,
    },
  },
  {
    timestamps: false,
    freezeTableName: true,
  }
);

// Remove the attributes that are auto created by Sequelize
kinara_engr_api_hist_table.removeAttribute("id"); // Primary Key Attribute
kinara_engr_api_hist_table.removeAttribute("createdAt"); // Created Date Attribute
kinara_engr_api_hist_table.removeAttribute("updatedAt"); // Update Date Attribute

/**
 * This async method will create a new table
 * with the above model else will update the
 * record in the table.
 */
// (async () => {
//   await kinara_engr_api_hist_table.sync();
// })();

export = kinara_engr_api_hist_table;
