/**
 * DESCRIPTION: CUSTOMER_API DATA MODEL USED TO
 *              DETAILS BASED ON THE PIN CODE
 * CREATED BY: VIKAS
 * CREATED DATE: 10/05/2021
 */

import { DataTypes, Model } from "sequelize";
import { DbConnection } from "../util/dbConfig";
const sequelize = new DbConnection().connectMySql();

// We recommend you declare an interface for the attributes, for stricter typechecking
// interface pinCodeAPIAttributes {
//   sales_officer_id: String;
//   sales_officer_Firstname: string;
//   sales_officer_Lastname: String;
//   role_designation: String;
//   zone_id: String;
//   zone_name: String;
//   state_id: String;
//   state_name: String;
//   division_id: String;
//   division_name: String;
//   region_id: String;
//   region_name: String;
//   hub_id: Number;
//   hubname: String;
//   spoke_id: Number;
//   spoke_name: String;
//   pincode: Number;
// }

interface pinCodeAPIInstance extends Model {
  sales_officer_id: String;
  sales_officer_Firstname: string;
  sales_officer_Lastname: String;
  role_designation: String;
  zone_id: String;
  zone_name: String;
  state_id: String;
  state_name: String;
  division_id: String;
  division_name: String;
  region_id: String;
  region_name: String;
  hub_id: Number;
  hubname: String;
  spoke_id: Number;
  spoke_name: String;
  pincode: Number;
}

// Some fields are optional when calling UserModel.create() or UserModel.build()
// interface pinCodeAPICreationAttributes
//   extends Optional<pinCodeAPIAttributes, "id"> {}

// // We need to declare an interface for our model that is basically what our class would be
// interface pinCodeAPIInstance
//   extends Model<pinCodeAPIAttributes, pinCodeAPICreationAttributes>,
//     pinCodeAPIAttributes {}

// Define pin_code_api ORM data model object
const pin_code_api = sequelize.define<pinCodeAPIInstance>(
  "pin_code_api",
  {
    sales_officer_id: {
      type: DataTypes.STRING(40),
      allowNull: true,
    },
    sales_officer_Firstname: {
      type: DataTypes.STRING(150),
      allowNull: true,
    },
    sales_officer_Lastname: {
      type: DataTypes.STRING(150),
      allowNull: true,
    },
    role_designation: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    zone_id: {
      type: DataTypes.STRING(10),
      allowNull: true,
    },
    zone_name: {
      type: DataTypes.STRING(35),
      allowNull: true,
    },
    state_id: {
      type: DataTypes.STRING(10),
      allowNull: true,
    },
    state_name: {
      type: DataTypes.STRING(35),
      allowNull: true,
    },
    division_id: {
      type: DataTypes.STRING(10),
      allowNull: true,
    },
    division_name: {
      type: DataTypes.STRING(35),
      allowNull: true,
    },
    region_id: {
      type: DataTypes.STRING(10),
      allowNull: true,
    },
    region_name: {
      type: DataTypes.STRING(35),
      allowNull: true,
    },
    hub_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    hubname: {
      type: DataTypes.STRING(35),
      allowNull: true,
    },
    spoke_id: {
      type: DataTypes.BIGINT,
      allowNull: true,
    },
    spoke_name: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    pincode: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
  },
  {
    timestamps: false,
    freezeTableName: true,
  }
);

// Remove the attributes that are auto created by Sequelize
pin_code_api.removeAttribute("id"); // Primary Key Attribute
pin_code_api.removeAttribute("createdAt"); // Created Date Attribute
pin_code_api.removeAttribute("updatedAt"); // Update Date Attribute

/**
 * This async method will create a new table
 * with the above model else will update the
 * record in the table.
 */
// (async () => {
//   await pin_code_api.sync();
// })();

export = pin_code_api;
