/**
 * DESCRIPTION: CUSTOMER_API DATA MODEL USED
 *              TO FETCH THE DEDUPE DETAILS
 * CREATED BY: VIKAS
 * CREATED DATE: 10/05/2021
 */

import { DataTypes, Model } from "sequelize";
import { DbConnection } from "../util/dbConfig";
const sequelize = new DbConnection().connectMySql();

// We recommend you declare an interface for the attributes, for stricter typechecking
interface DedupeDetailsInstance extends Model {
  customer_id: number;
  customer_name: string;
  date_of_birth: Date;
  urn_no: string;
  mobile_phone: string;
  whatsapp_no: string;
  identity_proof: string;
  id_proof_no: string;
  pan_card_no: string;
  door_no: string;
  street: string;
  landmark: string;
  pincode: number;
  district: string;
  state: string;
  village_name: string;
  locality: string;
  loan_id: number;
  relationship: string;
  hub_id: number;
  hub_name: string;
  spoke_id: number;
  spoke_name: string;
}

// Define Kinara_Engr_Api_DedupeApplicentDtl ORM data model object
const Kinara_Engr_Api_DedupeApplicentDtl =
  sequelize.define<DedupeDetailsInstance>(
    "Kinara_Engr_Api_DedupeApplicentDtl",
    {
      customer_id: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      customer_name: {
        type: DataTypes.STRING(100),
        allowNull: true,
      },
      date_of_birth: {
        type: DataTypes.DATE,
        allowNull: true,
      },
      urn_no: {
        type: DataTypes.STRING(16),
        allowNull: true,
      },
      mobile_phone: {
        type: DataTypes.STRING(15),
        allowNull: true,
      },
      whatsapp_no: {
        type: DataTypes.STRING(45),
        allowNull: true,
      },
      identity_proof: {
        type: DataTypes.STRING(100),
        allowNull: true,
      },
      id_proof_no: {
        type: DataTypes.STRING(30),
        allowNull: true,
      },
      pan_card_no: {
        type: DataTypes.STRING(16),
        allowNull: true,
      },
      door_no: {
        type: DataTypes.STRING(200),
        allowNull: true,
      },
      street: {
        type: DataTypes.STRING(200),
        allowNull: true,
      },
      landmark: {
        type: DataTypes.STRING(200),
        allowNull: true,
      },
      pincode: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      district: {
        type: DataTypes.STRING(100),
        allowNull: true,
      },
      state: {
        type: DataTypes.STRING(100),
        allowNull: true,
      },
      village_name: {
        type: DataTypes.STRING(100),
        allowNull: true,
      },
      locality: {
        type: DataTypes.STRING(100),
        allowNull: true,
      },
      loan_id: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      relationship: {
        type: DataTypes.STRING(45),
        allowNull: true,
      },
      hub_id: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      hub_name: {
        type: DataTypes.STRING(200),
        allowNull: true,
      },
      spoke_id: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      spoke_name: {
        type: DataTypes.STRING(200),
        allowNull: true,
      },
    },
    {
      timestamps: false,
      freezeTableName: true,
    }
  );

// Remove the attributes that are auto created by Sequelize
Kinara_Engr_Api_DedupeApplicentDtl.removeAttribute("id"); // Primary Key Attribute
Kinara_Engr_Api_DedupeApplicentDtl.removeAttribute("createdAt"); // Created Date Attribute
Kinara_Engr_Api_DedupeApplicentDtl.removeAttribute("updatedAt"); // Update Date Attribute

/**
 * This async method will create a new table
 * with the above model else will update the
 * record in the table.
 */
// (async () => {
//   await Kinara_Engr_Api_DedupeApplicentDtl.sync();
// })();

export = Kinara_Engr_Api_DedupeApplicentDtl;
