/**
 * CREATED BY: ANITA MS
 * CREATED DATE: 10/05/2021
 * MODIFIED BY:ANITA MS
 * MODIFIED DATE: 03/05/2021
 * DESCRIPTION: Kinara_Engr_Api_zoho_crm data model used to
 *              Define Kinara_Engr_Api_zoho_crm ORM data model object and
 *              to fetch the zoho details
 */

import { DataTypes, Model } from "sequelize";
import { DbConnection } from "../util/dbConfig";
const sequelize = new DbConnection().connectMySql();

// We recommend you declare an interface for the attributes, for stricter typechecking
interface ZohoAPIInstance extends Model {
  business_name: string;
  account_name: string;
  door_no: string;
  email: string;
  post_office: string;
  area: string;
  state: string;
  pincode: number;
  region_name: string;
  company_operating_since: Date;
  loan_Amount: number;
  business_type: string;
  customer_id: number;
  tenure: number;
  mobile_no: string;
  loan_id: number;
  no_of_installments_repaid: number;
  customer_bank: string;
  date_of_birth: Date;
  loan_subpurpose: string;
  firstPaymentDate: Date;
  hub_name: string;
  district: string;
  Gender: string;
  first_name: string;
  account_number: string;
  cbs_date: Date;
  customer_type: string;
  Bounce_if_any: string;
}

// Define Kinara_Engr_Api_zoho_crm ORM data model object
const Kinara_Engr_Api_zoho_crm = sequelize.define<ZohoAPIInstance>(
  "Kinara_Engr_Api_zoho_crm",
  {
    business_name: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    account_name: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    door_no: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    email: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    post_office: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    area: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    state: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    pincode: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    region_name: {
      type: DataTypes.STRING(35),
      allowNull: true,
    },
    company_operating_since: {
      type: DataTypes.DATE,
      allowNull: true,
    },
    loan_Amount: {
      type: DataTypes.DOUBLE,
      allowNull: true,
    },
    business_type: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    customer_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    tenure: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    mobile_no: {
      type: DataTypes.STRING(12),
      allowNull: true,
    },
    loan_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    no_of_installments_repaid: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    customer_bank: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    date_of_birth: {
      type: DataTypes.DATE,
      allowNull: true,
    },
    loan_subpurpose: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    firstPaymentDate: {
      type: DataTypes.DATE,
      allowNull: true,
    },
    hub_name: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    district: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    Gender: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    first_name: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    account_number: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    cbs_date: {
      type: DataTypes.DATE,
      allowNull: true,
    },
    customer_type: {
      type: DataTypes.STRING(10),
      allowNull: true,
    },
    Bounce_if_any: {
      type: DataTypes.STRING(3),
      allowNull: true,
    },
  },
  {
    timestamps: false,
    freezeTableName: true,
  }
);

// Remove the attributes that are auto created by Sequelize
Kinara_Engr_Api_zoho_crm.removeAttribute("id"); // Primary Key Attribute
Kinara_Engr_Api_zoho_crm.removeAttribute("createdAt"); // Created Date Attribute
Kinara_Engr_Api_zoho_crm.removeAttribute("updatedAt");

export = Kinara_Engr_Api_zoho_crm;
