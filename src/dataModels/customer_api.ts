/**
 * DESCRIPTION: CUSTOMER_API DATA MODEL USED
 *              TO FETCH THE CUSTOMER DETAILS
 * CREATED BY: VIKAS
 * CREATED DATE: 10/05/2021
 */

import { DataTypes, Model } from "sequelize";
import { DbConnection } from "../util/dbConfig";
const sequelize = new DbConnection().connectMySql();

// We recommend you declare an interface for the attributes, for stricter typechecking
interface CustomerAPIInstance extends Model {
  customer_id: number;
  account_number: string;
  customer_bank_name: string;
  area: string;
  branch_name: string;
  operating_since: Date;
  business_urn: string;
}

// Define customer_api ORM data model object
const customer_api = sequelize.define<CustomerAPIInstance>(
  "customer_api",
  {
    customer_id: {
      type: DataTypes.BIGINT,
      allowNull: true,
    },
    account_number: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    customer_bank_name: {
      type: DataTypes.STRING(300),
      allowNull: true,
    },
    area: {
      type: DataTypes.STRING(100),
      allowNull: true,
    },
    branch_name: {
      type: DataTypes.STRING(100),
      allowNull: true,
    },
    operating_since: {
      type: DataTypes.DATEONLY,
      allowNull: true,
    },
    business_urn: {
      type: DataTypes.STRING(100),
      allowNull: true,
    },
  },
  {
    timestamps: false,
    freezeTableName: true,
  }
);

// Remove the attributes that are auto created by Sequelize
customer_api.removeAttribute("id"); // Primary Key Attribute
customer_api.removeAttribute("createdAt"); // Created Date Attribute
customer_api.removeAttribute("updatedAt"); // Update Date Attribute

/**
 * This async method will create a new table
 * with the above model else will update the
 * record in the table.
 */
// (async () => {
//   await customer_api.sync();
// })();

export = customer_api;
