/**
 * DESCRIPTION: COLENDER_LOAN_PARAMETER DATA MODEL
 *              USED TO FETCH THE DIBURSED LOAN DETAILS
 *              FOR THE COLENDERS
 * CREATED BY: VIKAS
 * CREATED DATE: 10/05/2021
 */

import { DataTypes, Model } from "sequelize";
import { DbConnection } from "../util/dbConfig";
const sequelize = new DbConnection().connectMySql();

// Define colender_loan_parameters ORM data model object
const colender_loan_parameters = sequelize.define(
  "colender_loan_parameters",
  {
    loan_id: {
      type: DataTypes.INTEGER,
    },
    account_number: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    application_id: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    prinicipal_amount: {
      type: DataTypes.DOUBLE(22, 0),
      allowNull: true,
    },
    interest_amount: {
      type: DataTypes.DOUBLE(22, 0),
      allowNull: true,
    },
    disbursement_date: {
      type: DataTypes.DATEONLY,
      allowNull: true,
    },
    first_repayment_date: {
      type: DataTypes.DATEONLY,
      allowNull: true,
    },
    instrument_number: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    instrument_type: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    net_disbursement_amount: {
      type: DataTypes.DOUBLE(22, 0),
      allowNull: true,
    },
    installment_no: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    installment_amount: {
      type: DataTypes.DOUBLE(22, 0),
      allowNull: true,
    },
    next_repayment_date: {
      type: DataTypes.DATEONLY,
      allowNull: true,
    },
    mykinara_fieldname: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    mykinara_fieldname_url: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    mykinara_fieldname1: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    mykinara_fieldname1_url: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    mykinara_fieldname2: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    mykinara_fieldname2_url: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    mykinara_fieldname3: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    mykinara_fieldname3_url: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    mykinara_fieldname4: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    mykinara_fieldname4_url: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    mykinara_fieldname5: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    mykinara_fieldname5_url: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    mykinara_fieldname6: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    mykinara_fieldname6_url: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    mykinara_fieldname7: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    mykinara_fieldname7_url: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    mykinara_fieldname8: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    mykinara_fieldname8_url: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    mykinara_fieldname9: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    mykinara_fieldname9_url: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    mykinara_fieldname10: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    mykinara_fieldname10_url: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    mykinara_fieldname11: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    mykinara_fieldname11_url: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    mykinara_fieldname12: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    mykinara_fieldname12_url: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    mykinara_fieldname13: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    mykinara_fieldname13_url: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    mykinara_fieldname14: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    mykinara_fieldname14_url: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    mykinara_fieldname15: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    mykinara_fieldname15_url: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    mykinara_fieldname16: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    mykinara_fieldname16_url: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    mykinara_fieldname17: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    mykinara_fieldname17_url: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    mykinara_fieldname18: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    mykinara_fieldname18_url: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    mykinara_fieldname19: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    mykinara_fieldname19_url: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    mykinara_fieldname20: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    mykinara_fieldname20_url: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    mykinara_fieldname21: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    mykinara_fieldname21_url: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    mykinara_fieldname22: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    mykinara_fieldname22_url: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    mykinara_fieldname23: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    mykinara_fieldname23_url: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    mykinara_fieldname24: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    mykinara_fieldname24_url: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    mykinara_fieldname25: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    mykinara_fieldname25_url: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    mykinara_fieldname26: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    mykinara_fieldname26_url: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    mykinara_fieldname27: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    mykinara_fieldname27_url: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    mykinara_fieldname28: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    mykinara_fieldname28_url: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    mykinara_fieldname29: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    mykinara_fieldname29_url: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    mykinara_fieldname30: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    mykinara_fieldname30_url: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    mykinara_fieldname31: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    mykinara_fieldname31_url: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    mykinara_fieldname32: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    mykinara_fieldname32_url: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    mykinara_fieldname33: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    mykinara_fieldname33_url: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    mykinara_fieldname34: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    mykinara_fieldname34_url: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    mykinara_fieldname35: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    mykinara_fieldname35_url: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    mykinara_fieldname36: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    mykinara_fieldname36_url: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    mykinara_fieldname37: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    mykinara_fieldname37_url: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    mykinara_fieldname38: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    mykinara_fieldname38_url: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    mykinara_fieldname39: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    mykinara_fieldname39_url: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    mykinara_fieldname40: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    mykinara_fieldname40_url: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    mykinara_fieldname41: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    mykinara_fieldname41_url: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    mykinara_fieldname42: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    mykinara_fieldname42_url: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    mykinara_fieldname43: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    mykinara_fieldname43_url: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    mykinara_fieldname44: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    mykinara_fieldname44_url: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    mykinara_fieldname45: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    mykinara_fieldname45_url: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    mykinara_fieldname46: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    mykinara_fieldname46_url: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    mykinara_fieldname47: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    mykinara_fieldname47_url: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    mykinara_fieldname48: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    mykinara_fieldname48_url: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    mykinara_fieldname49: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    mykinara_fieldname49_url: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    mykinara_fieldname50: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    mykinara_fieldname50_url: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    mykinara_fieldname51: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    mykinara_fieldname51_url: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    mykinara_fieldname52: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    mykinara_fieldname52_url: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    mykinara_fieldname53: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    mykinara_fieldname53_url: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    loan_document_fieldname1: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    loan_document_fieldname1_id: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    loan_document_fieldname2: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    loan_document_fieldname2_id: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    loan_document_fieldname3: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    loan_document_fieldname3_id: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    loan_document_fieldname4: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    loan_document_fieldname4_id: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    loan_document_fieldname5: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    loan_document_fieldname5_id: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    loan_document_fieldname6: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    loan_document_fieldname6_id: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    loan_document_fieldname7: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    loan_document_fieldname7_id: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    loan_document_fieldname8: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    loan_document_fieldname8_id: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    loan_document_fieldname9: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    loan_document_fieldname9_id: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    loan_document_fieldname10: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    loan_document_fieldname10_id: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    loan_document_fieldname11: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    loan_document_fieldname11_id: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    loan_document_fieldname12: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    loan_document_fieldname12_id: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    loan_document_fieldname13: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    loan_document_fieldname13_id: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    loan_document_fieldname14: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    loan_document_fieldname14_id: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    loan_document_fieldname15: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    loan_document_fieldname15_id: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    loan_document_fieldname16: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    loan_document_fieldname16_id: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    loan_document_fieldname17: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    loan_document_fieldname17_id: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    loan_document_fieldname18: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    loan_document_fieldname18_id: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    loan_document_fieldname19: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    loan_document_fieldname19_id: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    loan_document_fieldname20: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
    loan_document_fieldname20_id: {
      type: DataTypes.STRING(200),
      allowNull: true,
    },
    co_lender: {
      type: DataTypes.STRING(50),
      allowNull: true,
    },
  },
  {
    timestamps: false,
    freezeTableName: true,
  }
);

// Remove the attributes that are auto created by Sequelize
colender_loan_parameters.removeAttribute("id"); // Primary Key Attribute
colender_loan_parameters.removeAttribute("createdAt"); // Created Date Attribute
colender_loan_parameters.removeAttribute("updatedAt"); // Update Date Attribute

/**
 * This async method will create a new table
 * with the above model else will update the
 * record in the table.
 */
// (async () => {
//   await colender_loan_parameters.sync();
// })();

export = colender_loan_parameters;
