/**
 * CREATED BY: RAKESH ANUGULA
 * CREATED DATE: 22/04/2021
 * DESCRIPTION:  OZONETEL CALL SUMMARY DETAILS DATA MODEL
 */

import { DataTypes, Model } from "sequelize";
import { DbConnection } from "../util/dbConfig";
const sequelize = new DbConnection().connectMySql();

//creating a ORM object for ozonetelCallSummaryDetail
const ozonetelCallSummaryDetail = sequelize.define(
  "ozonetelCallSummaryDetail",
  {
    AgentID: {
      type: DataTypes.STRING(20),
    },
    AgentName: {
      type: DataTypes.STRING(20),
    },
    AgentPhoneNumber: {
      type: DataTypes.STRING(20),
    },
    AgentStatus: {
      type: DataTypes.STRING(20),
    },
    AgentUniqueID: {
      type: DataTypes.STRING(20),
    },
    Apikey: {
      type: DataTypes.STRING(20),
    },
    AudioFile: {
      type: DataTypes.STRING(250),
    },
    CallDuration: {
      type: DataTypes.STRING(10),
    },
    CallerConfAudioFile: {
      type: DataTypes.STRING(250),
    },
    CallerID: {
      type: DataTypes.STRING(20),
    },
    CampaignName: {
      type: DataTypes.STRING(20),
    },
    CampaignStatus: {
      type: DataTypes.STRING(10),
    },
    Comments: {
      type: DataTypes.STRING(1000),
    },
    ConfDuration: {
      type: DataTypes.STRING(10),
    },
    CustomerStatus: {
      type: DataTypes.STRING(15),
    },
    DialStatus: {
      type: DataTypes.STRING(15),
    },
    DialedNumber: {
      type: DataTypes.STRING(20),
    },
    Did: {
      type: DataTypes.STRING(20),
    },
    Disposition: {
      type: DataTypes.STRING(200),
    },
    Duration: {
      type: DataTypes.STRING(10),
    },
    EndTime: {
      type: DataTypes.STRING(50),
    },
    FallBackRule: {
      type: DataTypes.STRING(25),
    },
    HangupBy: {
      type: DataTypes.STRING(20),
    },
    Location: {
      type: DataTypes.STRING(100),
    },
    monitorUCID: {
      type: DataTypes.STRING(50),
      primaryKey: true,
    },
    PhoneName: {
      type: DataTypes.STRING(20),
    },
    Skill: {
      type: DataTypes.STRING(50),
    },
    StartTime: {
      type: DataTypes.STRING(50),
    },
    Status: {
      type: DataTypes.STRING(15),
    },
    TimeToAnswer: {
      type: DataTypes.STRING(10),
    },
    TransferType: {
      type: DataTypes.STRING(20),
    },
    TransferredTo: {
      type: DataTypes.STRING(50),
    },
    Type: {
      type: DataTypes.STRING(20),
    },
    UserName: {
      type: DataTypes.STRING(20),
    },
    UUI: {
      type: DataTypes.STRING(20),
    },
  },
  {
    tableName: "Kinara_Engr_Api_Ozonetel_CallSummaryDetails",
  }
);

/**
 * This async method will do if table is not exist then it will create a new table with above model or otherwise
 * it will not table in database.
 */

// (async () => {
//   await ozonetelCallSummaryDetail.sync();
//   // sequelize.close();
// })();

export = ozonetelCallSummaryDetail;
