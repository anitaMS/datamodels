/**
 * DESCRIPTION: KINARA_ENGR_API_USERS DATA MODEL
 *              USED TO FETCH THE USER DETAILS
 * CREATED BY: VIKAS
 * CREATED DATE: 10/05/2021
 */

import { DataTypes, Model } from "sequelize";
import { DbConnection } from "../util/dbConfig";
const sequelize = new DbConnection().connectMySql();

// Define Kinara_Engr_Api_Users ORM data model object
const Kinara_Engr_Api_Users = sequelize.define(
  "Kinara_Engr_Api_Users",
  {
    owner_name: {
      type: DataTypes.STRING(50),
    },
    user_name: {
      type: DataTypes.STRING(50),
    },
    password: {
      type: DataTypes.STRING(100),
    },
    api_access_level: {
      type: DataTypes.STRING(500),
    },
    user_status: {
      type: DataTypes.STRING(20),
    },
  },
  {
    timestamps: false,
  }
);

/**
 * This async method will create a new table
 * with the above model else will update the
 * record in the table.
 */
// (async () => {
//   await Kinara_Engr_Api_Users.sync();
// })();

export = Kinara_Engr_Api_Users;
