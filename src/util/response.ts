/**
 * DESCRIPTION: CAPTURE HISTORY LOGS ON SUCCESS, FAILURE & VALIDATION FUNCTIONS
 * CREATED BY: VIKAS
 * CREATED DATE: 07/05/2021
 */

import { Request } from "express";

const APIHist = require("../util/addAPIHist");

/**
 * @desc    This file contain Success and Error response for sending to client / user
 */

/**
 * @desc    Send any success response
 *
 * @param   {number} statusCode
 * @param   {object | array} data
 * @param   {string} token
 * @param   {Json} req
 */
export class success {
  constructor(statusCode: number, data: object, req: Request) {
    const response = {
      status: statusCode,
      success: true,
      data,
    };

    return new Promise(async (resolve, reject) => {
      // Capture the details in a data table.
      await APIHist.add_api_call(
        req,
        response,
        "NA",
        "SUCCESS",
        "SUCCESS",
        statusCode
      );

      //Return the reponse.
      resolve(response);
    });
  }
}

/**
 * @desc    Send any error response
 *
 * @param   {string} API request token
 * @param   {Json} API request
 * @param   {Json} error
 * @param   {string} apiErrorLabel
 * @param   {string}
 * @param   {number} statusCode
 *
 */
export class error {
  constructor(
    req: Request,
    error: Response,
    apiErrorLabel: string,
    logicStatus: string,
    statusCode: number
  ) {
    // List of common HTTP request code
    const codes = [204, 400, 401, 404, 403, 422, 500];

    // Get matched code
    statusCode = codes.find((code) => code == statusCode) || 500;

    const response = {
      status: statusCode,
      success: false,
      error,
    };
    return new Promise(async (resolve, reject) => {
      // Capture the details in a data table.

      await APIHist.add_api_call(
        req,
        response,
        apiErrorLabel,
        "FAIL",
        logicStatus,
        statusCode
      );

      //Return the reponse.
      resolve(response);
    });
  }
}

/**
 * @desc    Send any validation response
 *
 * @param   {string} errors
 */
export class validation {
  constructor(errors: string) {
    return {
      message: "Validation errors",
      error: true,
      code: 422,
      errors,
    };
  }
}
