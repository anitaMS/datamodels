/**
 * DESCRIPTION: API CALL INSERTED INTO MYSQL DATABASE
 * CREATED BY: VIKAS
 * CREATED DATE: 11/05/2021
 */

import dateformat from "dateformat";
import { Request, Response } from "express";
import userModel from "../dataModels/Kinara_Engr_Api_Users";
import apiTokenModel from "../dataModels/Kinara_Engr_Api_Tokens";
import apiHistoryModel from "../dataModels/kinara_engr_api_hist_table";

/**
 * @desc This async function return user_name base on token.
 *
 * @param  {string} token Api token.
 * @return {JSON} user_name from database.
 */
async function get_user_id(request: Request, token: string) {
  //return new Promise(async (resolve, reject) => {
  const response = await apiTokenModel
    .findOne({
      attributes: ["user_Id"],
      where: { token: token },
      raw: true,
    })
    .then(async (result: any) => {
      return await userModel.findOne({
        attributes: ["user_name"],
        where: { ID: result.user_Id },
        raw: true,
      });
    });
  console.log(response);
  return response!;
  //resolve(response.user_name);
  //});
}

/**
 * In this we are save api calling in history tables.
 * Note: don't send server response.
 * @param  {json} req               APi request
 * @param  {json} res               APi response json, this should be json, created for api response.
 * @param  {String} apiErrorLabel   APi error reason, if success then NA,
 *                                  other reason eg. 500 --> internal error.
 * @param  {string} apiStatus       Api status  it can be success or fail.
 * @param  {string} logicStatus     Api logic status  it can be success or fail.
 * @param  {Number} apiStatusCode   Api status code, it can be 500, 200, etc.
 * @return {Boolean} true if successfully inserted otherwise false.
 */
module.exports.add_api_call = async (
  req: Request,
  res: Response,
  apiErrorLabel: string,
  apiStatus: string,
  logicStatus: string,
  apiStatusCode: number
) => {
  try {
    // Fetching user-id from user table, for this call async function
    // then handle problem, so written in then block.
    let token = <string>req.headers["authorization"]?.split(" ")[1];
    // if (token.length > 1) {
    //   token = token[1];
    // } else {
    //   token = token[0];
    // }

    // Get user-id
    let apiUserId_promise = get_user_id(req, token);
    await apiUserId_promise.then(async (result: any) => {
      // get user_name else "NA"
      let apiUserId = result.user_name || "NA";

      //console.log(apiUserId);
      let apiRoute = req.originalUrl;
      let apiName = apiRoute.split("/")[1];
      // considering last value of string, eg. /api/colending --> colending
      // apiName = apiName[apiName.length - 1];
      // taking server type from .env
      let apiSeverType = process.env.SERVER_TYPE;
      // Current Datetime with India local time zone
      let dateTime = dateformat(
        new Date(),
        "yyyy-mm-dd HH:MM:ss"
      ).toLocaleString();
      // .toLocaleString("en-US", {
      //   timeZone: "Asia/Calcutta",
      // });

      // Inserting api details in api_hist table,
      // for splitting  big inset query into 3 parts and merging them back.
      // Converting JSON (req and res) to string and replacing `"` to `'` while inserting value.
      let response = await apiHistoryModel.create({
        application_id: "N/A",
        user_id: apiUserId,
        api_route: apiRoute,
        api_name: apiName,
        api_error_label: apiErrorLabel,
        status_code: apiStatusCode,
        api_status: apiStatus,
        logic_status: logicStatus,
        api_request: JSON.stringify(req.body).replace(/"/g, `'`),
        api_response: JSON.stringify(res).replace(/"/g, `'`),
        server_type: apiSeverType,
        api_request_on: new Date(),
      });

      // Capture the Log
      console.log(response instanceof apiTokenModel);
      return response instanceof apiTokenModel;
    });
  } catch (ex) {
    console.log(`error ${ex}`);
  }
};
