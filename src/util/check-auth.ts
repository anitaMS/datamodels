/**
 * CREATED BY: Rakesh Anugula
 * CREATED DATE: 20/03/2021
 * DESCRIPTION: common function to verify token is valid or not.
 */
import { Request, Response, NextFunction } from "express";
import * as jwt from "jsonwebtoken";

export class CheckAuth {
  constructor(req: Request, res: Response, next: NextFunction) {
    try {
      const token = req.headers["authorization"]; //.split(" ")[1];
      console.log(token);

      // verifying the input token is valid or not
      const decoded = jwt.verify(<string>token?.split(" ")[1], "kinaracapital");
      res.locals.jwtPayLoad = decoded;
      //req.userData = decoded;
      next();
    } catch (error) {
      res.status(error.status || 500);
      res.status(401).json({
        message: "Auth failed",
      });
    }
  }
}
