/**
 * DESCRIPTION: SEQUELIZE ORM AND QUERY EXECUTION FUNCTION
 * CREATED BY: VIKAS
 * CREATED ON: 03/05/2021
 */

import { Request, Response } from "express";
import { Sequelize, QueryTypes } from "sequelize";
import { loggerDetails } from "./loggers";

//Database wide options
const opts = {
  define: {
    //Prevent sequelize from pluralizing table names
    freezeTableName: true,
  },
};

/**
 * @desc This return database connection pool,
 * in this if you send server type as false,
 * will allow to connection to external database like RDS.
 * default server type is true. which take from env file and return relevant
 * database connection.
 * created pool because this don't need to close database connection.
 *
 * @param {Number} PORT
 * @param {String} HOST
 * @param {String} USERNAME
 * @param {String} PASSWORD
 * @param {String} DATABASE
 */

export class DbConnection {
  constructor(
    public PORT?: number,
    public HOST?: string,
    public USERNAME?: string,
    public PASSWORD?: string,
    public DATABASE?: string
  ) {}

  /**
   * @desc Function that handles the connection sting
   */
  connectMySql() {
    console.log(
      this.PORT,
      this.HOST,
      this.DATABASE,
      this.USERNAME,
      this.PASSWORD
    );

    return new Sequelize(
      <string>this.DATABASE,
      <string>this.USERNAME,
      this.PASSWORD,
      {
        host: this.HOST,
        port: this.PORT,
        dialect: "mysql",
        timezone: "+05:30",
      }
    );
  }

  /**
   * @desc ExecuteQuery function to execute a sql query
   * and restun the resultant data using Sequelize and
   * close the DB connection.
   *
   * @param {request} req - api request
   * @param {string} sqlQuery - mysql query
   * @returns {array} result
   */
  ExecuteQuery(req: Request, sqlQuery: string) {
    return new Promise((resolve, reject) => {
      // Sequelize query to insert a record.
      if (sqlQuery.includes("INSERT")) {
        this.connectMySql()
          .query(sqlQuery, { type: QueryTypes.INSERT })
          .then((obj) => {
            resolve(obj);
            // Console the Logger
            loggerDetails(req, ` QUERY RESPONSE - ${JSON.stringify(obj)}`);

            // Closing Sequelize Connection.
            //Sequelize.close();
          })
          .catch((err) => {});
      }

      // Sequelize query to update record
      else if (sqlQuery.includes("UPDATE")) {
        this.connectMySql()
          .query(sqlQuery, { type: QueryTypes.UPDATE })
          .then((obj) => {
            resolve(obj);
            // Console the Logger
            loggerDetails(req, ` QUERY RESPONSE - ${JSON.stringify(obj)}`);

            // Closing Sequelize Connection.
            //Sequelize.close();
          })
          .catch((err) => {});
      }

      // Sequelize to execute the select query.
      else {
        this.connectMySql()
          .query(sqlQuery, { type: QueryTypes.SELECT })
          .then((obj) => {
            resolve(obj);
            // Console the Logger
            loggerDetails(req, ` QUERY RESPONSE - ${JSON.stringify(obj)}`);

            // Closing Sequelize Connection.
            //Sequelize.close();
          })
          .catch((err) => {});
      }
    });
  }
}
