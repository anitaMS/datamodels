/**
 * DESCRIPTION: CONSOLE THE LOG
 * CREATED BY: VIKAS
 * CREATED DATE: 05/05/2021
 */

import { Request } from "express";

/**
 * @desc Function to capture request and
 *       response log in console
 *
 * @type {Request} request
 * @type {string} message
 */
export function loggerDetails(req: Request, message: string) {
  if (req.originalUrl !== undefined) {
    // Fetch the URL from the request
    let apiName = req.originalUrl;
    console.log("" + apiName);
    let concData = apiName + "\n" + message;
    console.log(`dd ${message}`);
    let msg = concData.replace(/\n/g, "-->");
    console.log(msg);
  }
}
